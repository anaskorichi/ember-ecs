# Ember ECS

This is a very simple ECS implementation, meant as a learning resource.

However, feel free to use it in anything you want.

## Resources

These were used as a basis for creating this.

- (makrjs)[https://github.com/makrjs/makr]
- (EnTT)[https://github.com/skypjack/entt]
